﻿import pytest

def func(string):
    """
    Функция нахождения префикса имени.

    Принимает на вход имя.
    Находит префикс от имени.

    Args:
        string: Имя.

    Returns:
        Возвращает префтксы от имени.

    Raises:
        IndexError

    Examples:
        >>> func(Pavel)
        ["P", "Pa", "Pav", "Pave"]
        >>> func(213)
        IndexError: list index out of range
    """
    start = 0
    lst1= []
    i = 1
    while True:
        if start < len(string)-1:
            lst1.append(string[:i])
            i += 1
            start += 1
        else:
            break
    return lst1

def func_l(lname):
    """
    Функция нахождения префикса фамилии.

    Принимает на вход фамилию.
    Находит префикс от фамилии.

    Args:
        lname: Фамилия.

    Returns:
        Возвращает префтксы от фамилии.

    Raises:
        IndexError

    Examples:
        >>> func_l(Petrov)
        ["P", "Pe", "Pet", "Petr", "Petro"]
        >>> func(213)
        IndexError: list index out of range
    """
    start = 0
    lst = []
    i = 1
    while True:
        if start < len(lname)-1:
            lst.append(lname[:i])
            i += 1
            start += 1
        else:
            break
    return lst


if __name__ == "__main__":
    string = str(input('Input user name: '))
    lname = str(input('Input user lastname:'))
    a = func(string)
    b = func_l(lname)
    print(a[3] + b[0], 'successfully registered')

# Тест, в котором находится и преобразуются префиксы имени
def test1_test():
    assert func("Daniil") == ["D", "Da", "Dan", "Dani", "Danii"]
    assert func("Nikita") == ["N", "Ni", "Nik", "Niki", "Nikit"]
    assert func("Pavel") == ["P", "Pa", "Pav", "Pave"]

# Тест, в котором находится и преобразуются префиксы фамилии
def test2_test():
    assert l("Drugov") == ["D", "Dr", "Dru", "Drug", "Drugo"]
    assert l("Ivanov") == ["I", "Iv", "Iva", "Ivan", "Ivano"]
    assert l("Petrov") == ["P", "Pe", "Pet", "Petr", "Petro"]